/**
* Vérifie si deux tableaux n'ont aucune valeur commune
* @author SEVETTE Matiss
*/

class SontDiff {
        
    void principal() {
        testSontTousDiff();
    }

    /**
    * vérifie si deux tableaux n’ont aucune valeur commune
    * @param tab1 premier tableau
    * @param tab2 deuxième tableau
    * @return vrai si les tableaux tab1 et tab2 n’ont aucune valeur 
    * commune, faux sinon
    */
    boolean sontTousDiff (int[] tab1, int[] tab2) {
        int i = 0;
        int j = 0;
        boolean trouve = true;

        while (trouve == true && j < tab2.length) {
            i = 0;
            while(trouve == true && i < tab1.length){
                if (tab1[i] == tab2[j]) {
                    trouve = false;
                }
                i++;
            }
            j++;
        }
        return trouve;
    }
    
    void testSontTousDiff() {
        System.out.println ();
        System.out.println ("*** testSontTousDiff()");
        int[] tab1 = {1,2,3,4};
        int[] tab2 = {5,6,7,8};
        int[] tab3 = {1,6,7,8};
        int[] tab4 = {5,6,7,4};
        int[] tab5 = {5,2,3,8};
        int[] tab6 = {0,0,0,0};
        int[] tab7 = {1,2,3,4};
        
        testCasSontTousDiff(tab1, tab2, true);
        testCasSontTousDiff(tab1, tab3, false);
        testCasSontTousDiff(tab1, tab4, false);
        testCasSontTousDiff(tab1, tab5, false);
        testCasSontTousDiff(tab1, tab6, true);
        testCasSontTousDiff(tab1, tab7, false);
    }
    
    
    void testCasSontTousDiff(int[] tab1, int[] tab2, boolean resultat) {
        boolean resExec = sontTousDiff(tab1,tab2);
        
        if (resultat == resExec){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}
