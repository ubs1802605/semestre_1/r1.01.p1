/**
* Vérifie si deux tableaux n'ont aucune valeur commune
* @author SEVETTE Matiss
*/

class EliminerDouble {
        
    void principal() {
        testEliminerDouble();
    }
    
    /**
     * Triage du tableau
    */
    int[] trierTableau (int[] t) {
        for (int i = 0; i < t.length; i++) {
            for (int j = 0; j < t.length; j++) {
                if (t[i] < t[j]) {
                    int temp = t[i];
					t[i] = t[j];
					t[j] = temp;
				}
			}
		}
        return t;
    }
    
    /**
    * élimine les valeurs en plusieurs exemplaires dans un tableau
    * un élément présent plusieurs fois n’est plus qu’en un seul 
    * exemplaire
    * @param tab
     tableau d’entiers
    * @return le nombre d’éléments du tableau sans double
    */
    int eliminerDouble(int[] tab) {
        
        trierTableau(tab);
        
        int i = 0;
        int j = 0;
        int nbVal = 0;
        int[] tab2 = new int[tab.length];
        int k = 0;
  
        while (i < tab.length) {
            if (i == tab.length - 1) {
                nbVal = nbVal + 1;
                tab2[k] = tab[i];
                i++;
                
            } else {
                
                if (tab[i] == tab[i+1]) {
                    i++;
                } else {
                    nbVal = nbVal + 1;
                    tab2[k] = tab[i];
                    k++;                    
                    i++;
                }
            }
        }
        System.out.println(Arrays.toString(tab2));
        return nbVal;
    }
    
    void testEliminerDouble () {
        System.out.println ();
        System.out.println ("*** testEstSousChaine()");
        
        int[] tab1 = {0,1,2,3,4,5,5,1,2,3,4};
        int[] tab2 = {0,0,3,1,2,1};
        int[] tab3 = {0};
        int[] tab4 = {};
        int[] tab5 = {6,5,4,3,2,1};
        int[] tab6 = {0,0,1,1,2,2,3,3,4,4,5,5,6,6};

        testCasEliminerDouble(tab1, 6);
        testCasEliminerDouble(tab2, 4);
        testCasEliminerDouble(tab3, 1);
        testCasEliminerDouble(tab4, 0);
        testCasEliminerDouble(tab5, 6);
        testCasEliminerDouble(tab6, 7);
    }
    
    void testCasEliminerDouble(int[] tab, int n) {
        int result = eliminerDouble(tab);
        
        if (result == n){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}
