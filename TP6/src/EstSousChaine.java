/**
* Vérifie si un string est dans un autre string
* @author SEVETTE Matiss
*/

class EstSousChaine {
        
    void principal() {
        testEstSousChaine();
    }

    /**
    * teste si une chaîne est une sous-chaîne d’une autre
    * @param mot
     chaîne de caractères
    * @param phrase chaîne de carectères
    * @return vrai ssi la première chaîne est présente dans la seconde
    */
    boolean estSousChaine (String mot, String phrase) {
        
        int i = 0;
        int j = 0;
        boolean trouve = false;
        
        while (j < phrase.length() && trouve == false) {
            trouve = true;
            while (i < mot.length() && j < phrase.length() 
              && trouve == true) {
                if (mot.charAt(i) == phrase.charAt(j)) {
                    trouve = true;
                    i++;
                    j++;
                } else {
                    trouve = false;
                    i = 0;
                    j++;
                }
            } 
            if (i != mot.length()) {
                trouve = false;
            }
        }
        return trouve;
    }
    
    void testEstSousChaine () {
        System.out.println ();
        System.out.println ("*** testEstSousChaine()");

        testCasEstSousChaine("mot", "mot", true);
        testCasEstSousChaine("mot", "mogmot", true);
        testCasEstSousChaine("mot", "motgmo", true);
        testCasEstSousChaine("mot", "mo", false);
        testCasEstSousChaine("mot", "mogmo", false);
        testCasEstSousChaine("ses", "abcdsesdef", true);
        testCasEstSousChaine("ses", "abcdef", false);
        testCasEstSousChaine("ses", "abcdefse", false);
    }
    
    void testCasEstSousChaine (String mot, String phrase, 
      boolean result) {
        boolean resExec = estSousChaine(mot,phrase);
        
        if (result == resExec){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}
