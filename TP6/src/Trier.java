/**
* Entrer des valeurs et les trier directement dans un tableau
* @author SEVETTE Matiss
*/
import java.util.Arrays;

class Trier {
    
    final int LG_TAB = 10;
    
    void principal() {
        testSaisirEtTrier();
    }

    
    /*** Créer et saisit un tableau trié de LG_TAB entiers
    * @return tableau trié de LG-TAB entiers
    */
    int[] saisirEtTrier () {
        int[] t = new int[LG_TAB];
        int i = 0;
        
        while (i < t.length) {
            t[i] = SimpleInput.getInt ("Entrer un entier ");
            
            int j = i;
            int tmp;
            boolean superieur = false;
            while (j > 0 && superieur == false) {
                if (t[j] < t[j-1]){
                    tmp = t[j];
                    t[j] = t[j-1];
                    t[j-1] = tmp;
                } else {
                    superieur = true;
                }
                j--;
            }
            i++;
        }
        return t;
    }
    
    void testSaisirEtTrier() {
        System.out.println ();
        System.out.println ("*** testSaisirEtTrier ()");
        
        int[] res1 = {1,2,3,4,5,6,7,8,9,10};
        testCasSaisirEtTrier("Taper 1,2,3,4,5,6,7,8,9,10", res1);
        testCasSaisirEtTrier("Taper 10,9,8,7,6,5,4,3,2,1", res1);
        testCasSaisirEtTrier("Taper 2,1,3,4,5,6,7,8,9,10", res1);
        testCasSaisirEtTrier("Taper 1,2,3,4,5,6,7,8,10,9", res1);
        testCasSaisirEtTrier("Taper 1,2,3,4,6,5,7,8,10,9", res1);
        
        
        int[] res2 = {-2,4,4,5,6,7,8,9,10,24};
        testCasSaisirEtTrier("Taper -2,4,4,5,6,7,8,9,10,24", res2);
        testCasSaisirEtTrier("Taper 4,-2,4,5,6,7,8,9,10,24", res2);
        testCasSaisirEtTrier("Taper -2,4,4,5,6,7,8,9,24,10", res2);
        testCasSaisirEtTrier("Taper -2,4,4,5,7,6,8,9,24,10", res2);
        
        int[] res3 = {1,1,1,1,1,1,1,1,1,1};
        testCasSaisirEtTrier("Taper 1,1,1,1,1,1,1,1,1,1", res3);
    }
    
    void testCasSaisirEtTrier (String msg, int[] res) {
        System.out.println(msg);
        
        int[] resExec = saisirEtTrier();
        
        if (Arrays.equals(res, resExec)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}
