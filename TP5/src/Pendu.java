/**
* Lancement du jeu du pendu
* @author SEVETTE Matiss
*/
import java.util.Arrays;

class Pendu {
    void principal() {
        partie(creerDico());
    }

/**
* Création d’un dictionnaire de mots
* @return dictionnaire initialisé
*/
String[] creerDico () {
        String[] dico = {"SOURIS", "CLAVIER", "ECRAN", "ORDINATEUR"};
        return dico;
    }

/**
* Test de la méthode creerDico
* Le test nous dit combien de mots comporte le tableau
*/
    void testCreerDico () {
        System.out.println ();
        System.out.println ("*** testCreerDico ()");
	
        if (creerDico().length == 0) {
            System.out.println("ATTENTION ! Le dictionnaire n'a " 
              + "pas de mot, il va être difficile de jouer");
        } 
        System.out.println(Arrays.toString(creerDico()));
    }
   
    
/**
* choix aléatoire d’un mot dans un dictionnaire
* @param dico dictionnaire des mots à choisir
* @return chaine choisie de manière aléatoire
*/
    String choisirMot (String[] dico) {
        double random = Math.random()*4;
        int i = (int) random;
        
        String motChoisi = dico[i];
        
        return motChoisi;
    }


/**
* Test de choisirmot()
*/
    void testChoisirMot (String[] dico) {
        System.out.println ();
        System.out.println ("*** testChoisirMot ()");
        
        double random = Math.random()*4;
        int i = (int) random;
    
        for (i = 0; i < 15; i++) {
            System.out.println(choisirMot(dico));
        }
    }
    
    
/**
* affiche la réponse du joueur
* @param reponse reponse du joueur
*/
    void afficherReponse(char[] reponse) {
        for (int i = 0 ; i < reponse.length; i++) {
            System.out.print (reponse[i] + " ");
        }
        System.out.println ();
    }
    
    
/**
* Test de afficherReponse()
*/
    void testAfficherReponse () {
        System.out.println ();
        System.out.println ("*** testAfficherReponse()");
        
        char[] reponse1 = {'P', 'R', 'O', 'G', 'R', 'A', 'M', 'M', 'E'};
        testCasAfficherReponse (reponse1);
        char[] reponse2 = {};
        testCasAfficherReponse (reponse2);
    }
    
/**
* teste un appel à afficherReponse()
*/
    void testCasAfficherReponse (char[] reponse) {
        System.out.print ("afficherReponse (" + Arrays.toString(reponse) 
          + ") : ");
        afficherReponse(reponse);
    }
    
    
/**
* création d’un tableau de reponse contenant des ’_’
* @param lg longueur du tableau à créer
* @return tableau de reponse contenant des ’_’
*/
    char[] creerReponse(int lg) {
        char[] rep = new char [lg];
        int i = 0;
        
        while (i < lg) {
            rep[i] = '_';
            System.out.print(rep[i] + " ");
            i++;   
        }
        return rep;
    }
    
/**
* Test de creerReponse()
*/
    void testCreerReponse () {
        System.out.println ();
        System.out.println ("*** testCreerReponse()");

        testCasCreerReponse(0);
        testCasCreerReponse(5);
        testCasCreerReponse(1);
        testCasCreerReponse(7);
    }
    
/**
* teste un appel à creerReponse()
*/
    void testCasCreerReponse (int lg) {
        System.out.print("creerReponse : ");
        creerReponse(lg);
        System.out.println();
    }
    
    
/**
* teste la présence d’un caractère dans le mot
* et le place au bon endroit dans réponse
* @param mot mot à deviner
* @param reponse réponse à compléter si le caractère est présent 
dans le mot
* @param car caractère à chercher dans le mot
* @return vrai ssi le caractère est dans le mot à deviner
*/
    boolean tester(String mot, char[] reponse, char car) {
        
        boolean trouve = false;
        int i = 0;
    
        while (i < mot.length()) {
            if(car == mot.charAt(i)) {
                trouve = true;
                reponse[i] = car;
            } 
            i++;
        }
        
        i = 0;
        while (i < reponse.length) {
            System.out.print(reponse[i] + " ");
            i++;
        }
        return trouve;
    }
    
/**
* Test de tester()
*/
    void testTester () {
        System.out.println ();
        System.out.println ("*** testTester()");
        
        char[] rep1 = {'_', '_', '_', '_', '_'};
        char[] rep2 = {'_', '_', '_', '_', '_', '_'};

        testCasTester("ECRAN", rep1, 'E', true);
        testCasTester("SOURIS", rep2, 'E', false);
        testCasTester("SOURIS", rep2, ' ', false);
        testCasTester("SOURIS", rep2, 'u', false);
    }
    
/**
* teste un appel à tester()
*/
    void testCasTester (String mot, char[] reponse, char car, 
        boolean result) {
        
        boolean resExec = tester(mot, reponse, car);
        System.out.print(" : ");
        
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    
/**
* rend vrai ssi le mot est trouvé
* @param mot mot à deviner
* @param reponse réponse du joueur
* @return vrai ssi le mot est égal caractère par caractère à la 
* réponse
*/
    boolean estComplet (String mot, char[] reponse) {
        boolean complet = true;
        int i = 0;

        while(i < mot.length() && complet == true) {
            if (mot.charAt(i) != reponse[i]) {
                complet = false;
            }
            i++;
        }
        return complet;
    }    


/**
* teste la méthode estComplet
**/ 
    void testEstComplet () {
        System.out.println ();
        System.out.println ("*** testTester()");

        char[] rep1 = {'E','C','R','A','N'};
        char[] rep2 = {'S','O','U','R', 'I', 'S'};
        char[] rep3 = {'S','_','_','R', 'I', 'S'};

        testCasEstComplet("ECRAN", rep1, true);
        testCasEstComplet("SOURIS", rep2, true);
        testCasEstComplet("souris", rep2, false);
        testCasEstComplet(" ", rep1, false);
        testCasEstComplet("SOURIS ", rep3, false);
    }
    
/**
* teste un appel de estComplet
**/ 
    void testCasEstComplet(String mot, char[] reponse, boolean result){
        System.out.print("Est Complet ? : ");
   
        boolean resExec = estComplet(mot, reponse);
       
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
/**
* lancement d’une partie du jeu du pendu
* @param dico dictionnaire des mots à deviner
*/
    void partie(String[] dico) {
        String mot = choisirMot(dico);
        char[] reponse = creerReponse(mot.length());
        int nbEssais = 9;
        while (nbEssais > 0 && estComplet(mot, reponse) == false) {
            char car = SimpleInput.getChar("Essayez une lettre en "
            + "majuscule: ");
            if (tester(mot, reponse, car) == true) {
                System.out.println("Bien vu");
            } else {
                nbEssais = nbEssais - 1;
                System.out.println("Aïe ! Il reste " + nbEssais 
                + " essais restants");
            }
            afficherReponse(reponse);
        }
        if (estComplet(mot, reponse)) {
            System.out.println("Bravo, tu as trouvé le mot !");
        } else {
            System.out.println("Dommage, tu as perdu !");
        }
    }
}
