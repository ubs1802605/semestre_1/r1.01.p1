/**
* Calcul de la factorielle et des combinaisons
* @author SEVETTE Matiss
*/

class Factorielle {
	
	void principal() {
		
		testFactorielle();
		testCombinaison();
	}	


	/**
	* calcul de la factorielle du paramètre
	* @param n valeur de la factorielle à calculer
	* @return resultat (factoriel de n)
	*/
	int factorielle(int n){
		
		int i = n - 1;
		if (n == 0) {
			n = 1;
		}	
		
		int resultat = n;
		while (i > 1) {
			resultat = resultat * i;
			i = i - 1;
		}
		return resultat;
	}


	//calcul de la combinaision
	int combinaison (int k, int n) {
		int resultat;
		
		resultat = (factorielle(n))/(factorielle(k)*factorielle((n-k)));
		
		return resultat;
	}


	
	//Teste de la méthode testCasFactorielle
	void testFactorielle () {
		System.out.println ();
		System.out.println ("*** testFactorielle()");
	
		testCasFactorielle (5, 120);
		testCasFactorielle (0, 1);
		testCasFactorielle (1, 1);
		testCasFactorielle (2, 2);
	}
	
	//testCasFactorielle
	/**
	* teste un appel de factoriel
	* @param n valeur de la factorielle à calculer
	* @param result resultat attendu
	**/
	void testCasFactorielle (int n, int result) {
		System.out.print ("factorielle (" + n + ") \t= " + result + 
		  "\t : ");
		
		int resExec = factorielle(n);
		
		if (resExec == result){
		System.out.println ("OK");
		} else {
		System.err.println ("ERREUR");
		}
	}
	
	//Test de la combinaison
	void testCombinaison () {
		System.out.println ();
		System.out.println ("*** testCombinaison()");
	
		testCasCombinaison (3, 5, 10);
		testCasCombinaison (2, 4, 6);
		testCasCombinaison (3, 6, 20);
		testCasCombinaison (0, 0, 1);
		testCasCombinaison (1, 1, 1);
		testCasCombinaison (1, 2, 2);
		testCasCombinaison (25, 24, -2);
	}
	
	

	/**
	* teste un appel de combinaison
	* @param n valeur de la factorielle à calculer
	* @param result resultat attendu
	**/
	void testCasCombinaison (int k, int n, int result) {
		System.out.print ("Combinaison de " + k + " parmis " + n + 
		  " : ");

		int resExec = combinaison(k,n);
	
		if (resExec == result){
			System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}

}

