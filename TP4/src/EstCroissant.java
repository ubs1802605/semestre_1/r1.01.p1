/**
* Vérification si un tableau est croissant
* @author SEVETTE Matiss
*/

class EstCroissant {
	void principal() {
		testEstCroissant();
    }
        
    
        
    /**
    * teste si les valeurs d’un tableau sont triées par ordre croissant
    * @param t tableau d’entiers
    * @return vrai ssi les valeurs du tableau sont en ordre croissant
    */
    boolean estCroissant (int[] t) {
        
        //Vérification si le tableau est dans l'ordre croissant ou non
        boolean croissant = true;
        
		if (t.length != 0) {
            int nbVal = t.length;
			int i = 0;
				
			
			while(i < (nbVal - 1) && croissant == true) {
				if (t[i+1] < t[i]) {
					croissant = false;					
				}
				
				i = i + 1;
			}
	
        } else {
            croissant = false;
		}
        return croissant;
	}
    
    
    void testEstCroissant () {
        System.out.println ();
        System.out.println ("*** testEstCroissant ()");
	
        testCasEstCroissant(new int[] {1,2,3}, true);
        testCasEstCroissant(new int[] {1,2,3,4,6,5}, false);
        testCasEstCroissant(new int[] {2,1,3,4,5,6}, false);
        testCasEstCroissant(new int[] {1}, true);
        testCasEstCroissant(new int[] {3,2,1}, false);
        testCasEstCroissant(new int[] {}, false);
    }
    
    
    void testCasEstCroissant (int[] t, boolean result) {
        System.out.print("Est croissant ? :");
         
        boolean resExec = estCroissant(t);
            
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}
