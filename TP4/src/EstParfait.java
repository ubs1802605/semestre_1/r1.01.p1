/**
* teste la divisibilité de deux entiers
* @author SEVETTE Matiss
*/

class EstParfait {
	
	void principal() {
        int a = SimpleInput.getInt("Choisir un nombre pour tester s'" +
          "il est parfait : ");
        
        System.out.println("La réponse est : " + estParfait(a));
        
        testEstParfait();
    }

    /**
    * teste si un nombre est parfait
    * @param a entier positif
    * @return vrai ssi a est un nombre parfait
    */
    boolean estParfait (int a) {
        int i = 1;
        int result = 0; 
        int reste; 
        boolean estParfait = false;
        
        while (i < a && a != 0) {
            reste = a % i;
            
            if (reste == 0) {
                result = result + i;
            }
            
            i = i + 1;
        }
        
        if (result == 0) {
            estParfait = false;
        } else if (result == a) {
            estParfait = true;
        }
            
        return estParfait;
    }
    
    /**
    * Teste la méthode estParfait()
    */
	void testEstParfait () {
		System.out.println ();
		System.out.println ("*** testEstParfait ()");
	
		testCasEstParfait(6, true);
        testCasEstParfait(28, true);
        testCasEstParfait(496, true);
        testCasEstParfait(10, false);
        testCasEstParfait(0, false);
     
	}
    
    /**
	* teste un appel de estParfait
    * @param a entier positif 
	* @param result resultat attendu
	**/ 
    void testCasEstParfait (int a, boolean result) {
        System.out.print(a + " est parfait : ");
         
        boolean resExec = estParfait(a);
            
        if (resExec == result){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
}
