/**
* teste la divisibilité de deux entiers
* @author SEVETTE Matiss
*/

class QuatreNbParfaits {
	
	void principal() {
        
        quatreNbParfait();
        
    }

    /**
    * Affiche les quatre premiers nombres parfaits
    */
    void quatreNbParfait () {
        int i = 1;
        int a = 1;
        int result = 0; 
        int reste; 
        int nbParfait = 0; 
      
      /**
       * Jusqu'à temps que le programme a trouvé 4 nbParfait : 
       * Pour chaque nombre a en commençant par 1, on trouve tous ses
       * diviseurs et on les additionne. Si c'est un nbParfait, 
       * on l'affiche et on recommence avec le a suivant.
       * 
       * A chaque fois que trouve ou que l'on ne trouve pas
       * un nbParfait, l'on augmente a de 1 et on remet i à 1 pour 
       * retrouver tout les diviseurs de a. Et ainsi de suite jusqu'à
       * avoir 4 nbParfait.
      */
        while (nbParfait < 4) {
            
             while (i < a) {
                reste = a % i;
                
                if (reste == 0) {
                    result = result + i;
                }
                
                i = i + 1;
            }
        
            if (result == a) {
                System.out.println("Nombre Parfait " + (nbParfait+1) + 
                  " : " + a);
                nbParfait = nbParfait + 1;
                a = a + 1;
                i = 1;
                result = 0;
            } else {
                result = 0;
                a = a + 1;
                i = 1;
            }
        } 
            
    }

}
