/**
* teste la divisibilité de deux entiers
* @author SEVETTE Matiss
*/

class EstDiviseur {
	
	void principal() {
        int p = SimpleInput.getInt("Choisir un nombre pour tester la " +
          "divisibilité : ");
        int q = SimpleInput.getInt("Demande si le nombre est diviseur" +
          " de : ");
        
        System.out.println("La réponse est : " + estDiviseur(p,q));
        
        testEstDiviseur();
    }

    /**
    * teste la divisibilité de deux entiers
    * @param q entier positif à tester pour la divisibilité
    * @param p diviseur strictement positif
    * @return vrai ssi p divise q
    */
    boolean estDiviseur (int p, int q) {
        boolean divise = false;
        
        if (p != 0) {
            int reste = (q % p);
            if (reste == 0) {
                divise = true;
            }
        } else if (p == 0){
            divise = false;
        }
        
        return divise;
    }
    
    
    /**
    * Teste la méthode estDiviseur()
    */
	void testEstDiviseur () {
		System.out.println ();
		System.out.println ("*** testEstDiviseur ()");
	
		testCasEstDiviseur(5,15, true);
        testCasEstDiviseur(5,11, false);
        testCasEstDiviseur(15, 5, false);
        testCasEstDiviseur(16, 2, false);
        testCasEstDiviseur(2, 16, true);
        testCasEstDiviseur(2, 15, false);
        testCasEstDiviseur(5, 0, true);
        testCasEstDiviseur(0, 5, false);
	}
	

	/**
	* teste un appel de estDiviseur
    * @param p entier positif à tester pour la divisibilité
    * @param q diviseur strictement positif
	* @param result resultat attendu
	**/ 
	void testCasEstDiviseur (int p, int q, boolean result) {
        if (p == 0) {
            System.out.print(p + " est diviseur de " + q + " : " +
              "INTERDIT : "); 
            System.out.println("Division par 0 impossible");
        } 
        else {
            System.out.print(p + " est diviseur de " + q + " : ");
         
            boolean resExec = estDiviseur(p,q);
            
            if (resExec == result){
                System.out.println ("OK");
            } else {
                System.err.println ("ERREUR");
            }
        }
    }
}
