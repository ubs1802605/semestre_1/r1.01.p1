/**
* Le programme s'arrête quand on saisit un nombre inférieur au précédent
* @author SEVETTE Matiss
*/

class SaisieInferieure {
	void principal() {
		
		double val1;
		double val2;
		
		//Rentrer les deux premières valeurs
		val1 = SimpleInput.getFloat ("Valeur 1 : ");
		val2 = SimpleInput.getFloat ("Nouvelle valeur : ");
		
		//Tant que la deuxième valeur est >= à la première...
		while (val2 >= val1) {
			
			//La première valeur prend la valeur de la deuxième
			val1 = val2; 
			
			/*Et on redemande la valeur 2 vu que la valeur 1 a pris la 
			valeur 2*/
			val2 = SimpleInput.getInt ("Nouvelle valeur : ");
		}
		
		System.out.println("La nouvelle valeur est inférieure à la "
		  + "précédente");
	}
}
