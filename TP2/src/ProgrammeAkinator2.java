/**
* Le programme doit deviner un nombre choisi par l'utilisateur + déduire
* @author SEVETTE Matiss
*/

class ProgrammeAkinator2 {
	
	void principal() {
		
		int tentative;
		char reponse;
		int max = 1000;
		int min = 0;
		
		
		//L'ordi choisi un nombre entre 0 et 1000
		tentative = (int) ((Math.random() * (max - min)) + min);
		
		//On demande à l'utilisateur si son nombre est -, + ou =
		System.out.println("Ton nombre est-il  -, + ou = à : " 
		  + tentative);
		reponse = SimpleInput.getChar ("-, +, = ? : ");
		
		//Tant que l'utilisateur n'a pas entré '='
		while (reponse != '=') {
		
			/*
			* Si le nombre donné par l'ordi est supérieur au nombre
			* choisi par l'utilisateur, alors le nombre max que peut 
			* donner l'ordi devient le nombre de sa tentative.
			* "-1" car sinon l'ordi peut redemander le même nombre max 
			* en boucle alors qu'on lui a spécifié que ce n'était pas 
			* ce nombre.
			*/
			
			if (reponse == '-') {
				max = tentative - 1;
			}  
			
			
			/*
			* Si le nombre donné par l'ordi est inférieur au nombre
			* choisi par l'utilisateur, alors le nombre min que peut 
			* donner l'ordi devient le nombre de sa tentative.
			* "+1" car sinon l'ordi peut redemander le même nombre min 
			* en boucle alors qu'on lui a spécifié que ce n'était pas 
			* ce nombre.
			*/
			if (reponse == '+') {
				min = tentative + 1;
			} 
			
			
			//On redemande un nombre entre le nouveau max ou min
			tentative = (int) ((Math.random() * (max - min)) + min);
			
			
			/*
			 * Supposons que le nombre soit 701
			 * Supposons aussi que le programme donne 700, 
			 * min = tentative + 1 = 701
			 * Supposons maintenant que le programme donne 702,
			 * max = tentative - 1 = 701
			 * 
			 * Ici, si min et max sont différents, l'ordi nous redemande
			 * Sinon si min et max sont identiques OU min == 0 
			 * OU max == 1000, l'ordi déduit le nombre et sort du while
			 */
			if (min != max) {
				System.out.println("Le nouveau nombre est : " 
				  + tentative);
				reponse = SimpleInput.getChar ("-, +, = ? : ");
			} else if (min == max || min == 1000 || max == 0) {
				reponse = '=';
				System.out.println("Par déduction : " + tentative);
			}
		}
		System.out.println("Le résultat était : " + tentative);
	}
}
