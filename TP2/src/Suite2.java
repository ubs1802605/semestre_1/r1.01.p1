/**
* Suite de nombre qui s'arrête quand on rentre -1 et qui calcule la moyenne
* @author Matiss SEVETTE
**/


class Suite2 {
	
	void principal(){
		
		//definition
		double nb;
		double somme = 0;
		int tour = 0;
		double moyenne = 0;
		
		
		//Entree du nombre
		nb = SimpleInput.getInt("Nombre = ");
		
		//Programme 
		while (nb != -1) {
			System.out.println(nb);
			tour = tour + 1;
			somme = somme + nb;
			nb = SimpleInput.getInt("Nombre = ");
		}
		
		if (somme == 0) {
			System.out.println("Pas de moyenne car pas de note");
		} else {
			moyenne = somme / tour;
			System.out.println("Moyenne = " + moyenne);
		}
	}
}
