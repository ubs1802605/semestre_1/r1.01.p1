/**
* Calcul de la puissance d'un réel
* @author SEVETTE Matiss
*/

class CalculPuissance{
	
	void principal() {
		
		//Le réel x
		double x;
		
		//L'exposant n
		int n;
		double resultat;
		
		/*
		 * tour = 1 et non tour = 0 car, si n est différent de 0, 
		 * on considère que le premier tour est directement x¹
		*/
		int tour = 1;

		//Entrée du réel et de l'exposant
		x = SimpleInput.getFloat ("Valeur du réel x : ");
		n = SimpleInput.getInt ("Valeur de l'exposant entier n : ");
		
		/*
		 * On est obligé d'initialiser "resultat" car sinon on ne peut
		 * pas rentrer dans le while, donc ici resultat = x¹
		*/
		resultat = x;
		
		
		/*Si n == 0, resultat = 1 et tour = 0 pour ne pas rentrer dans 
		 * while*/
		if (n == 0) {
			resultat = 1;
			tour = 0;
		}
		
		/*
		 * Si n < 0, on inverse le signe de n pour que la condition du 
		 * while fonctionne ("tour" est toujours positif donc on doit 
		 * avoir n positif), 
		 * Puis (x = 1 / x) car (x^-1 = 1/x) et on réinitialise 
		 * "resultat"
	   	*/ 
		if (n < 0) {
			n = -n;
			x = 1 / x;
			resultat = x;
		}
		
		
		/*
		 * Tant que tour est différent de n, on multiplie
		 * le réel par lui-même et on ajoute 1 à "tour"
		*/ 
		while (tour != n) {
			resultat = resultat * x;
			tour = tour + 1;
		}
		
		System.out.println("Résultat : " + resultat);
	}
