/**
* Le programme doit deviner un nombre choisi par l'utilisateur
* @author SEVETTE Matiss
*/

class ProgrammeAkinator1 {
	
	void principal() {
		
		int nb;
		int tentative;
		char reponse;
		int max = 1000;
		int min = 0;
		
		
		//L'ordi choisi un nombre entre 0 et 1000
		tentative = (int) ((Math.random() * (max - min)) + min);
		
		//On demande à l'utilisateur si son nombre est -, + ou =
		System.out.println("Ton nombre est-il  -, + ou = à : " 
		  + tentative);
		reponse = SimpleInput.getChar ("-, +, = ? : ");
		
		//Tant que l'utilisateur n'a pas entré '='
		while (reponse != '=') {
		
			/*
			* Si le nombre donné par l'ordi est supérieur au nombre
			* choisi par l'utilisateur, alors le nombre max que peut 
			* donner l'ordi devient le nombre de sa tentative.
			* "-1" car sinon l'ordi peut redemander le même nombre max 
			* en boucle alors qu'on lui a spécifié que ce n'était pas 
			* ce nombre.
			*/
			if (reponse == '-') {
				max = tentative - 1;
			} 
			
			/*
			* Si le nombre donné par l'ordi est inférieur au nombre
			* choisi par l'utilisateur, alors le nombre min que peut 
			* donner l'ordi devient le nombre de sa tentative.
			* "+1" car sinon l'ordi peut redemander le même nombre min 
			* en boucle alors qu'on lui a spécifié que ce n'était pas 
			* ce nombre.
			*/
			if (reponse == '+') {
				min = tentative + 1;
			}
			
			//On redemande un nombre entre le nouveau max ou min
			tentative = (int) ((Math.random() * (max - min)) + min);
			System.out.println("Le nouveau nombre est : " + tentative);
			reponse = SimpleInput.getChar ("-, +, = ? : ");
		}
		System.out.println("Le résultat était : " + tentative);
	}
}
