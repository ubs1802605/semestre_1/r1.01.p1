/**
* Suite de nombre qui s'arrête quand on rentre -1
* @author Matiss SEVETTE
**/


class Suite1 {
	
	void principal(){
		
		//definition
		int nb;
		
		//Entree du nombre
		nb = SimpleInput.getInt("Nombre = ");
		
		//Programme 
		while (nb != -1) {
			System.out.println(nb);
			nb = SimpleInput.getInt("Nombre = ");
		}
	}
}
