/**
* Rôle à deviner
* @author SEVETTE Matiss
*/
class RoleADeviner {
	void principal() {
		
		int val1 = 0;
		int val2 = 0;
		
		

		while (val1 <= 0) {
			val1 = SimpleInput.getInt ("Première valeur (>0) : ");
		}
		
		while (val2 <= 0) {
			val2 = SimpleInput.getInt ("Deuxième valeur (>0) : ");
		}
		
		while (val1 != val2) {
			if (val1 > val2) {
				val1 = val1 - val2;
			} else {
				val2 = val2 - val1;
			}
		}
		System.out.println("Le résultat est : " + val1);
	}
}
