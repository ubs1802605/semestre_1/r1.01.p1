/**
* Devine le nombre entre 0 et 100
* @author SEVETTE Matiss
*/

class DevineLeNombre {
	
	void principal() {
		
		int nb;
		int tentative;
		
		/*
		*tour = 1 car la première fois que l'utilisateur tente de 
		*trouver le nombre, il n'est pas dans le while et donc
	    *sa tentative n'est pas comptée
		*/
		int tour = 1;
		
		//L'ordi prend au hasard un nombre entre 0 et 100
		nb = (int) (Math.random() * 100);
		
		//On demande à l'utilisateur de rentrer un nombre
		tentative = SimpleInput.getInt ("Essaye de trouver le nombre " 
		  + "entre 0 et 100 : ");
		
		
		//Tant que le nombre rentré est différent du nombre de l'ordi
		while (tentative != nb) {
			//on augmente le nombre de tour de 1
			tour = tour + 1;
			
			/*
			 * Si la tentative est inférieure au nombre de l'ordi, la 
			 * console nous renvoie "Trop petit"
			 * Sinon si la tentative est supérieure au nombre de l'ordi,
			 * elle nous renvoie "Trop grand"
			 * 
			 * Puis on redemande une tentative
			 */
			if (tentative < nb) {
				System.out.println("Trop petit");
			} else if (tentative > nb) {
				System.out.println("Trop grand");
			}
			tentative = SimpleInput.getInt ("Essaye encore : ");
		}
		
		System.out.println("Bravo ! Le nombre était " + nb);
		System.out.println("Nombre d'essais " + tour);

	}
}
