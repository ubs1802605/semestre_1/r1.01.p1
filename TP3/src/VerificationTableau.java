/**
* Saisie d'une valeur et vérification si cette valeur est présente
* au moins deux fois dans un tableau
* @author SEVETTE Matiss
*/

class VerificationTableau {
	void principal() {
		int[] t = {1};
		System.out.println(estCroissant(t));
		

        
    }
        
    /**
    * teste si les valeurs d’un tableau sont triées par ordre croissant
    * @param t tableau d’entiers
    * @return vrai ssi les valeurs du tableau sont en ordre croissant
    */

    boolean estCroissant (int[] t) {
        
        //Vérification si le tableau est dans l'ordre croissant ou non
        boolean croissant = true;
        
		if (t.length != 0) {
            int nbVal = t.length;
			int i = 0;
				
			//(nbVal-1) car sinon, si on met 10 valeurs, (t[i+1] < t[i])
			while(i < (nbVal - 1) && croissant == true) {
				if (t[i+1] < t[i]) {
					croissant = false;					
				}
				
				i = i + 1;
			}
	
        } else {
            croissant = false;
		}
        return croissant;
	}
}
