/**
* Saisie d'un String et vérification de si c'est un palindrome ou non
* @author SEVETTE Matiss
*/

class Palindrome {
	void principal() {
		
		String mot = SimpleInput.getString ("Mot ou nombre (minuscule)"
		  + " : ");
		int i = 0;
		boolean idem = true;
		
		/*
		 * Tant que i est inférieur au mot/nombre divisé par 2 ET que 
		 * idem == true  
		*/ 
		while (i < (mot.length()/2) && idem == true) {
			
			/*
			 * Ici on vérifie si le (premier caractère + i) est égal au
			 * (dernier caractère - 1 - i) (-1 car sinon ce n'est pas le 
			 * dernier caractère mais la longueur du mot, et rappelons
			 * que le mot commence par le caractère 0)
			*/ 
			if (mot.charAt((mot.length()-mot.length()) + i) 
			  == (mot.charAt((mot.length() - 1) - i))) {
				idem = true;
			} else {
				idem = false;
			}
			
			i = i + 1;
			}
			
		// Si c'est idem, on affiche que le mot/nombre est un palindrome
		if (idem == true) {
			System.out.println(mot + " est un mot palindrome"); 
		} else if (idem == false) {
			System.out.println(mot + " n'est pas un mot palindrome");
		}
	}
}


