/**
* Saisie d'entiers dans un tableau et arrêt quand -1
* @author SEVETTE Matiss
*/

class TableauEntiers {
	void principal() {
		
		int[] t = new int [10];
		int i = 0;
		int nbVal = 0;
		int nb = 0;
		
		while (i < t.length && nb !=-1) {
			nb = SimpleInput.getInt ("tab[" + i + "] = ");
			t[i] = nb;
			
			if (nb != -1) {
				i = i + 1;
			}
		}
		
		nbVal = i;
		System.out.println(nbVal + " valeurs ont été saisies"); 
	}
}
