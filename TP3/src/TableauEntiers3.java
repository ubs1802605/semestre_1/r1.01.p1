/**
* Saisie d'entiers dans un tableau et arrêt quand -1, puis on affiche
* le tableau, puis demande d'une valeur et on vérifie si elle est dans 
* le tableau
* @author SEVETTE Matiss
*/

class TableauEntiers3 {
	void principal() {
		
		int[] t = new int [10];
		int i = 0;
		int nb = 0;
		
		/*
		 * Tant que la longueur maximale du tableau n'est pas atteinte
		 * ET que le nombre saisi est différent de 1 :
		 * On demande une valeur nb qu'on affecte au tableau à l'indice
		 * i, et si le nombre n'est pas -1 (pour avoir i = pile le 
		 * nombre de valeur rentrée dans le tableau) on rajoute 1 à i 
		*/
		while (i < t.length && nb !=-1) {
			nb = SimpleInput.getInt ("nb = " );
			t[i] = nb;
			if (nb != -1) {
				i = i + 1;
			}
		}
		
		//Le nombre de valeurs rentrées prend la valeur de i 
		int	nbVal = i;
		System.out.println(nbVal + " valeurs ont été saisies"); 
		
		
		//On remet i à O pour afficher les valeurs une par une
		i = 0;
		while (i < nbVal){
			System.out.println("tab[" + i + "] = " + t[i]);
			i = i + 1;
		}
		
		
		/*
		 * Maintenant on veut savoir si une certaine valeur qui nous
		 * est demandée est dans le tableau
		 * 
		 * On regarde d'abord si on a bien rentré au moins une valeur
		 * dans le tableau pour continuer, sinon le programme nous dit
		 * qu'on ne peut pas chercher une valeur dans un tableau vide
		*/
		if (nbVal != 0) {
			
		
			/*
			 * Ici on remet i à 0 et on considère un boolean à false qui 
			 * deviendra true quand la valeur qu'on entre est trouvée 
			 * dans le tableau, puis on ajoute 1 à i pour vérifier 
			 * chaque valeur du tableau
			*/ 
			boolean trouvee = false;
			i = 0;
			int ask = SimpleInput.getInt("Demande d'une valeur : ");
			
			while(i < nbVal && trouvee == false) {
				if (ask == t[i]) {
					trouvee = true;
				} else if (ask != t[i]) {
					trouvee = false;
				}
				
				i = i + 1;
			}
			
			/*
			 * Si la valeur qu'on a rentré est trouvée, on affiche 
			 * qu'elle est trouvée, sinon l'inverse
			 */ 
			if (trouvee == true) {
				System.out.println("La valeur est présente");
			}
				
			if (trouvee == false) {
				System.out.println("La valeur n'est pas présente dans "
				  + "t ");
			}
		} 
		
		else {
			System.out.println("Aucune valeur donc pas de demande");
		}
	}
}
