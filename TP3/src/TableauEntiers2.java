/**
* Saisie d'entiers dans un tableau et arrêt quand -1, puis on affiche
* @author SEVETTE Matiss
*/

class TableauEntiers2 {
	void principal() {
		
		int[] t = new int [10];
		int i = 0;
		int nbVal = 0;
		int nb = 0;
		
		while (i < t.length && nb !=-1) {
			nb = SimpleInput.getInt ("nb = " );
			t[i] = nb;
			if (nb != -1) {
				i = i + 1;
			}
		}
		
		nbVal = i;
		System.out.println(nbVal + " valeurs ont été saisies"); 
		
		//On remet i à O pour afficher les valeurs une par une
		i = 0;
		while (i < nbVal){
			System.out.println("tab[" + i + "] = " + t[i]);
			i = i + 1;
		}
	}
}
