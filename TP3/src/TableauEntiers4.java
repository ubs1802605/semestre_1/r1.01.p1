/**
* Saisie d'entiers dans un tableau et arrêt quand -1, puis vérification
* si les valeurs sont dans l'ordre croissant ou non.
* @author SEVETTE Matiss
*/

class TableauEntiers4 {
	void principal() {
		
		int[] t = new int [10];
		int i = 0;
		int nbVal = 0;
		int nb = 0;
		
		while (i < t.length && nb !=-1) {
			nb = SimpleInput.getInt ("tab[" + i + "] = ");
			t[i] = nb;
			
			if (nb != -1) {
				i = i + 1;
			}
		}
		
		nbVal = i;
		System.out.println(nbVal + " valeurs ont été saisies"); 
		
		
		//Vérification si le tableau est dans l'ordre croissant ou non
		if (nbVal != 0) {
				
			boolean croissant = true;
			i = 0;
				
			//(nbVal-1) car sinon, si on met 10 valeurs, (t[i+1] < t[i])
			while(i < (nbVal - 1) && croissant == true) {
				if (t[i+1] < t[i]) {
					croissant = false;					
				}
				
				i = i + 1;
			}
	
			
			if (croissant == true) {
				System.out.println("Les valeurs du tableau sont "
				  + "dans l'ordre croissant.");
			} else if (croissant == false) {
				System.out.println("Les valeurs du tableau ne sont "
				  + "pas dans l'ordre croissant.");
			}
		}
		else {
			System.out.println("Aucune valeur donc pas d'ordre" 
			  + " croissant.");
		}
	}
}
