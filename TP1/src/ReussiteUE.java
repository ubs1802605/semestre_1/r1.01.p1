/**
* Savoir si tu as eu ton année
* @author Matiss SEVETTE
**/


class ReussiteUE {
	
	void principal(){
		
		//definition competances
		float ue1, ue2, ue3, ue4, ue5, ue6;
		
		//definition si la moyennes est au dessus de 10 pour chaque ue
		boolean m1 = false;
		boolean m2 = false;
		boolean m3 = false;
		boolean m4 = false;
		boolean m5 = false;
		boolean m6 = false;
		
		//Entree des notes
		ue1 = SimpleInput.getFloat("UE1 = ");
		ue2 = SimpleInput.getFloat("UE2 = ");
		ue3 = SimpleInput.getFloat("UE3 = ");
		ue4 = SimpleInput.getFloat("UE4 = ");
		ue5 = SimpleInput.getFloat("UE5 = ");
		ue6 = SimpleInput.getFloat("UE6 = ");
		
		//Savoir si la moyenne est au dessus de 10 dans chaque ue
		if (ue1 >= 10) {
			m1 = true;
		}
		
		if (ue2 >= 10) {
			m2 = true;
		}
		
		if (ue3 >= 10) {
			m3 = true;
		}
		
		if (ue4 >= 10) {
			m4 = true;
		}
		
		if (ue5 >= 10) {
			m5 = true;
		}
		
		if (ue6 >= 10) {
			m6 = true;
		}
			
			
		//Savoir combien de moyennes sont au dessus de 10
		int moyenneTotale = 0;
		
		if (m1 == true) {
			moyenneTotale = moyenneTotale + 1;
		}
		
		if (m2 == true) {
			moyenneTotale = moyenneTotale + 1;
		}
		
		if (m3 == true) {
			moyenneTotale = moyenneTotale + 1;
		}
		
		if (m4 == true) {
			moyenneTotale = moyenneTotale + 1;
		}
		
		if (m5 == true) {
			moyenneTotale = moyenneTotale + 1;
		}
		
		if (m6 == true) {
			moyenneTotale = moyenneTotale + 1;
		}

		System.out.println(
		"Nombre de moyennes au dessus ou égale à 10 = " 
		  + moyenneTotale);
		
		
		//Si moins de 4 moyennes sont au dessus de 10, l'élève n'a pas
		//son année
		if (moyenneTotale < 4) {
			System.out.println("Moins de 4 moyennes au dessus ou égales" 
			  + " à 10, désolé mais tu n'as pas ton année..."); 
		}
		
		
		//Nombre de moyenne entre 8 et moins de 10
		int nbreMoyenneEntre8Et10 = 0;
		
		if (ue1 >= 8 && ue1 < 10) {
			nbreMoyenneEntre8Et10 = nbreMoyenneEntre8Et10 + 1;
		}
		
		if (ue2 >= 8 && ue2 < 10) {
			nbreMoyenneEntre8Et10 = nbreMoyenneEntre8Et10 + 1;
		}
		
		if (ue3 >= 8 && ue3 < 10) {
			nbreMoyenneEntre8Et10 = nbreMoyenneEntre8Et10 + 1;
		}
		
		if (ue4 >= 8 && ue4 < 10) {
			nbreMoyenneEntre8Et10 = nbreMoyenneEntre8Et10 + 1;
		}
		
		if (ue5 >= 8 && ue5 < 10) {
			nbreMoyenneEntre8Et10 = nbreMoyenneEntre8Et10 + 1;
		}
		
		if (ue6 >= 8 && ue6 < 10) {
			nbreMoyenneEntre8Et10 = nbreMoyenneEntre8Et10 + 1;
		}
		
		System.out.println("Nombre de moyennes entre 8 et moins de 10 =" 
		  + nbreMoyenneEntre8Et10);
		  
		  
		//Si la moyenne de 8 n'est pas obtenue dans toutes les 
		//compétances, l'élève n'a pas son année
		boolean moyInf8 = false;
		
		if (ue1 < 8 || ue2 < 8 || ue3 < 8 || ue4 < 8 || ue5 < 8 || 
			ue6 < 8) {
			System.out.println("La moyenne de 8 n'est pas obtenue " 
			  + "dans toutes les compétances, "
			  + "désolé mais tu n'as pas ton année...");
			moyInf8 = true;
		} 


		//Si l'élève a obtenu son année
		if (moyenneTotale >= 4 && moyInf8 == false) {
			System.out.println("Bravo ! tu as obtenu ton année !");
		}
	}
}

