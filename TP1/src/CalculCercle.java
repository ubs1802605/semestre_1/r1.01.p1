/**
* Calcul du perimetre et du rayon d'un cercle en cm
* @author Matiss SEVETTE
**/


class CalculCercle {
	
	void principal(){

		float val1; 

		val1 = SimpleInput.getFloat("Rayon du cercle en cm = ");
		
		while (val1 < 0){
			System.out.println("Veuillez saisir un rayon positif ");
			val1 = SimpleInput.getFloat("Rayon du cercle = ");
		} 
		double perimetre = val1 * 2 * Math.PI;
		double aire = Math.PI * (val1*val1);
			
		System.out.println("Le périmètre d'un cercle de " + val1 
		  + "cm est " + perimetre + "cm et l'aire est " 
		  + aire + "cm²."); 
	}
}


	
	
	
