
/**
* TP R1.01.P2 - Semaine 50 et 51
* @author SEVETTE Matiss
*/
import java.util.*;

import javax.xml.transform.Source;

import java.io.*;

class Test {
	void principal() {
		int[] tab = {5,2,4,3,1};
		unAlgo(tab, 5);
		System.out.println(Arrays.toString(tab));
	}
	
	/**
		 * Rôle à déterminer
		 * 
		 * @param tab un tableau d’entiers
		 * @param n   le nombre d’entiers dans le tableau
		 */
		void unAlgo(int[] tab, int n) {
			int k, m, tmp;

			for (int i = 0; i < (n - 1); i++) {
				m = tab[i];
				k = i;
				for (int p = (i + 1); p < n; p++) {
					if (tab[p] < m) {
						m = tab[p];
						k = p;
					}
				}
				tmp = tab[k];
				tab[k] = tab[i];
				tab[i] = tmp;
			}
		}
}
