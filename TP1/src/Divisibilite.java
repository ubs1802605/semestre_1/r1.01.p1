/**
* Savoir si un nombre est divisible par 2,3,4 ou 5 ou aucun
* @author Matiss SEVETTE
**/


class Divisibilite {
	
	void principal(){

		//Si le nombre n'est pas divisible par...
		boolean nonDiv2 = false;
		boolean nonDiv3 = false;
		boolean nonDiv4 = false;
		boolean nonDiv5 = false;
		
		int nombre = SimpleInput.getInt("Nombre = ");
		
		while (nombre == 0) {
			System.out.println(
			  "Veuillez saisir un nombre supérieur à 0");
			nombre = SimpleInput.getInt("Nombre = ");
		}
			//divisible par 2
			int reste_2 = (nombre % 2);
			if (reste_2 == 0) {
				System.out.println(nombre + " est divisible par 2 !");
			} else {
			nonDiv2 = true;
			System.out.println(nombre + " n'est pas divisible par 2.");
			}
			
			//divisible par 3
			int reste_3 = (nombre % 3);
			if (reste_3 == 0) {
				System.out.println(nombre + " est divisible par 3 !");
			} else {
			nonDiv3 = true;
			System.out.println(nombre + " n'est pas divisible par 3.");
			}
				
			//divisible par 4
			int reste_4 = (nombre % 4);
			if (reste_4 == 0) {
				System.out.println(nombre + " est divisible par 4 !");
			} else {
			nonDiv4 = true;
			System.out.println(nombre + " n'est pas divisible par 4.");
			}	
			
			//divisible par 5
			int reste_5 = (nombre % 5);
			if (reste_5 == 0) {
				System.out.println(nombre + " est divisible par 5 !");
			} else {
			nonDiv5 = true;
			System.out.println(nombre + " n'est pas divisible par 5.");
			}
			
			//divisible ni par 2,3,4 ou 5
			if (nonDiv2 == true && nonDiv3 == true && nonDiv4 == true 
			  && nonDiv5 == true) {
				System.out.println(nombre + 
				  " n'est ni divisible par 2,3,4 ou 5.");
			}

	} 		
}

