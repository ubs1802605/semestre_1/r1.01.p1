/**
* Calcul d'une fiche de salaire a partir d'un salaire brut
* @author Matiss SEVETTE
**/


class CalculSalaire {
	
	void principal(){

		float salaireBrut; 

		salaireBrut = SimpleInput.getFloat("Salaire brut = ");
		
		while (salaireBrut < 0){
			System.out.println("Veuillez saisir un salaire positif ");
			salaireBrut = SimpleInput.getFloat("Salaire brut = ");
		} 
		
		//pourcentage des prevelements
		float am, avd, csg, crds, avp, chomage, deductionFraisPro; 
		
		//montants des prelevements compare au total
		float p_am, p_avd, p_csg, p_crds, p_avp, p_chomage;
		
		am = 0.75F/100;
		avd = 0.1F/100;
		csg = 7.5F/100;
		crds = 0.5F/100;
		avp = 6.75F/100;
		chomage = 2.4F/100;
		deductionFraisPro = 1.75F/100;
		
		p_am = salaireBrut * am;
		p_avd = salaireBrut * avd;
		p_csg = salaireBrut * csg;
		p_crds = (salaireBrut - (salaireBrut*deductionFraisPro)) 
		  *  crds;
		p_avp = (salaireBrut - (salaireBrut*deductionFraisPro)) 
		  *  avp;
		p_chomage = salaireBrut * chomage;
		
		//total prelevement
		float totalPrelevement = p_am + p_avd + p_csg + p_crds + p_avp
		  + p_chomage;
		
		//salaire net
		float salaireNet = salaireBrut - totalPrelevement;
		
			
		System.out.println("Le salaire brut est de " 
		  + salaireBrut + "€");
		 
		System.out.println("L'assurance maladie prend " + 100*am 
		  + "%, donc ici " + p_am + "€");
		System.out.println("L'assurance vieillesse déplafonné prend " 
		+ 100*avd + "%, donc ici " + p_avd + "€");
		System.out.println("La contribution social généralisée prend " 
		  + 100*csg 
		  + "% après déduction des frais professionnels, donc ici " 
		  + p_csg + "€");
		System.out.println("La CRDS prend " + 100*crds 
		  + "% après déduction des frais professionnels, donc ici " 
		  + p_crds + "€");
		System.out.println("L'assurance vieillesse plafonnée prend " 
		  + 100*avp + "%, donc ici " + p_avp + "€");
		System.out.println("La part pour le chomage prend " 
		  + 100*chomage + "%, donc ici " + p_chomage + "€");
		  
		System.out.println();
		
		System.out.println("Le total des prélèvements est de " 
		  + totalPrelevement + "€");
		  
		System.out.println("Le salaire net est de " + salaireNet + "€");
	}
}
